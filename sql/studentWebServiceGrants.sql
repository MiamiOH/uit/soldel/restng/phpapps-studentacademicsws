/*****************************************************************************
  FILE NAME:  studentWebServiceGrants.sql
  Copyright (c) 2015 Miami University, All Rights Reserved.
  
  Miami University grants you ("Licensee") a non-exclusive, royalty free,
  license to use, modify and redistribute this software in source and
  binary code form, provided that i) this copyright notice and license
  appear on all copies of the software; and ii) Licensee does not utilize
  the software in a manner which is disparaging to Miami University.
  
  This software is provided "AS IS" and any express or implied warranties,
  including, but not limited to, the implied warranties of merchantability
  and fitness for a particular purpose are disclaimed. It has been tested
  and is believed to work as intended within Miami University's
  environment. Miami University does not warrant this software to work as
  designed in any other environment.
  
  
  AUTHOR:   Kent Covert
  
  DESCRIPTION:  Creates SQL grants needed for student academics web service
  
  AUDIT TRAIL:
  
  PRJ-TSK          Date        UniqueID      Version
                   7-Oct-15    COVERTKA      1.0
  Description
    Creation of the create table script
  
  AUDIT TRAIL: END
 *****************************************************************************/


REM
REM Spool the results to see what happened
REM

SPOOL aceElcTransition_grant_acess.lst

REM
REM Connect to the database properly to ensure that this table is created
REM   within the right schema.
REM

ACCEPT  database PROMPT 'Enter instance: ';
ACCEPT  baninst1_password PROMPT 'Enter password for baninst1 in &&database: ' HIDE;
CONNECT baninst1/&&baninst1_password@&&database;

SET ECHO ON

grant select on sgrsatt to muws_gen;
grant select on sgvsatt to muws_gen;
grant select on sorlcur to muws_gen;
grant select on stvlevl to muws_gen;
grant select on stvcamp to muws_gen;
grant select on stvclas to muws_gen;
grant select on stvcoll to muws_gen;
grant select on stvdegc to muws_gen;
grant select on stvstyp to muws_gen;
grant select on stvadmt to muws_gen;
grant select on sorlfos to muws_gen;
grant select on stvmajr to muws_gen;
grant execute on baninst1.PZ_ACE_Load to MUWS_GEN;
grant execute on baninst1.PZ_ELC4_Load to MUWS_GEN;
grant execute on baninst1.PZ_ELC5_Load to MUWS_GEN;

REM
REM     Turn off spool file
REM

SPOOL OFF

UNDEFINE database
UNDEFINE baninst1_password

REM table MU_ACCTGEN.CAW_REQUESTED_COURSE created