<?php

namespace MiamiOH\PhpAppsStudentAcademicWs\Services;

class Student extends \MiamiOH\RESTng\Service {

  private $datasource_name = 'MUWS_GEN_PROD';

  public function getStudent() {
    $request = $this->getRequest();
    $response = $this->getResponse();
    
    $uniqueId = $request->getResourceParam('uniqueId');
    
    $options = $request->getOptions();
    if (!isset($options['mode'])) {
      $options['mode'] = 'LEARNER';
    }

    if (!isset($options['termCode'])) {
          $options['termCode'] = 'current';
    }

    if (isset($options['termCode']) &&
        $options['termCode'] != 'current' &&
        $options['termCode'] != 'max' &&
        !preg_match('/^\d\d\d\d\d\d$/', $options['termCode'])) {

      $response-setPayload(array('message' => 'Invalid termCode option'));
      $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
      return $response;
    }
    
    $authUser = $this->getApiUser()->getUsername();

    if (strtolower($authUser) != strtolower($uniqueId)) {
      if (!$this->getApiUser()->isAuthorized('WebServices', 'StudentAcademics', 'view') &&
          !$this->getApiUser()->isAuthorized('WebServices', 'StudentAcademics', 'All')) {
        $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
        return $response;
      }
    }
    
    $dbh = $this->database->getHandle($this->datasource_name);
    $dbh->mu_trigger_error = false;

    $pidm = $dbh->queryfirstcolumn("SELECT szbuniq_pidm FROM szbuniq WHERE szbuniq_unique_id = ?", strtoupper($uniqueId));
    if ($pidm === DB_EMPTY_SET) {
      $response->setPayload(array('message' => 'User not found'));
      $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
      return $response;
    }
    
    $currentTerm = $dbh->queryfirstcolumn("SELECT fz_get_term() FROM dual");
    if ($currentTerm === DB_EMPTY_SET) {
      $response->setPayload(array('message' => 'Unexpected error getting current term'));
      $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
      return $response;
    }

    $returnArray = array();

    $dResponse = $this->callResource('student.academics.v1.degree',
      array('params'  => array('uniqueId' => $uniqueId),
            'options' => $options));
    if ($dResponse->getStatus() !== \MiamiOH\RESTng\App::API_OK) {
      $response->setPayload(array('message' => 'Unexpected error when retrieving degrees'));
      $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
      return $response;
    }
    $dPayload = $dResponse->getPayload();

    $aResponse = $this->callResource('student.academics.v1.attributes',
      array('params'  => array('uniqueId' => $uniqueId),
            'options' => array('termCode' => $options['termCode'])));
    if ($aResponse->getStatus() !== \MiamiOH\RESTng\App::API_OK) {
      $response->setPayload(array('message' => 'Unexpected error when retrieving attributes'));
      $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
      return $response;
    }
    $aPayload = $aResponse->getPayload();

    // Primary campus and level should come from the first circulum record
    $returnArray['campusCode'] = ($dPayload[0]['campusCode'] ? $dPayload[0]['campusCode'] : '');
    $returnArray['campusName'] = ($dPayload[0]['campusName'] ? $dPayload[0]['campusName'] : '');
    $returnArray['levelCode']  = ($dPayload[0]['levelCode']  ? $dPayload[0]['levelCode']  : '');
    $returnArray['levelName']  = ($dPayload[0]['levelName']  ? $dPayload[0]['levelName']  : '');

    $class = $dbh->queryfirstrow_assoc(
      "SELECT fz_class_calc(?, ?, ?, '')
                                AS class_code,
              stvclas_desc      AS class_name
         FROM dual
    LEFT JOIN stvclas
           ON stvclas_code = fz_class_calc(?, ?, ?, '')",
      $pidm,
      $returnArray['levelCode'],
      (!$options['termCode'] || $options['termCode'] == 'current' || $options['termCode'] == 'max' ? $currentTerm : $options['termCode']),
      $pidm,
      $returnArray['levelCode'],
      (!$options['termCode'] || $options['termCode'] == 'current' || $options['termCode'] == 'max' ? $currentTerm : $options['termCode'])
    );

    $returnArray['classCode']  = trim($class['class_code']);
    $returnArray['className']  = trim($class['class_name']);

    // Check for conditionalAdmitProgramCode
    $returnArray['conditionalAdmitProgramCode'] = '';
    $returnArray['conditionalAdmitProgramName'] = '';
    foreach($aPayload as $attribute) {
      if ($attribute['code'] == 'ACE') {
        $returnArray['conditionalAdmitProgramCode'] = 'ACE';
        $returnArray['conditionalAdmitProgramName'] = 'American Culture and English Program';
      }
      if ($attribute['code'] == 'RELC') {
        $returnArray['conditionalAdmitProgramCode'] = 'ELC4';
        $returnArray['conditionalAdmitProgramName'] = 'English Learning Center Program - Level 4';
      }      
    }
    foreach($dPayload as $degree) {
      if ($degree['degreeCode'] == '999' &&
          $degree['levelCode'] == 'CR' &&
          $degree['admitTypeCode'] == 'RI' &&
          $degree['collegeCode'] == 'NC') {
        $returnArray['conditionalAdmitProgramCode'] = 'ELC1-3';
        $returnArray['conditionalAdmitProgramName'] = 'English Learning Center Program - Level 1-3';
      }        
    }

    $returnArray['degrees']    = $dPayload;
    $returnArray['attributes'] = $aPayload;

    $response->setStatus(\MiamiOH\RESTng\App::API_OK);
    $response->setPayload($returnArray);

    return $response;
  }

  private function getConditionalAdminProgramCode($uniqueId) {
    if (!$this->dbh) {
      $this->dbh = $this->database->getHandle($this->datasource_name);
    }

    $attribute = $this->dbh->queryfirstcolumn(
     "SELECT sgrsatt_atts_code
        FROM sgrsatt, szbuniq
        WHERE szbuniq_unique_id = ?
          AND szbuniq_pidm = sgrsatt_pidm
          AND sgrsatt_atts_code IN ('ACE', 'RELC')
          AND sgrsatt_term_code_eff = (
            SELECT max(sgrsatt_term_code_eff)
              FROM sgrsatt
             WHERE sgrsatt_pidm = szbuniq_pidm)",
      strtoupper($uniqueId));
    if ($attribute == 'ACE') {
      return 'ACE';
    } else if ($attribute == 'RELC') {
      return 'ELC4';
    } else {
      $elc13 = $this->dbh->queryfirstcolumn(
       "SELECT sorlcur_pidm
          FROM sorlcur, szbuniq
         WHERE szbuniq_unique_id = ?
           and szbuniq_pidm = sorlcur_pidm
           AND sorlcur_lmod_code = 'LEARNER'
           AND sorlcur_cact_code = 'ACTIVE'
           AND sorlcur_degc_code = 999
           AND sorlcur_levl_code = 'CR'
           AND sorlcur_admt_code = 'RI'
           AND sorlcur_coll_code = 'NC'
           AND sorlcur_term_code = 
             (SELECT MAX(sorlcur_term_code)
                FROM sorlcur
               WHERE sorlcur_pidm = szbuniq_pidm)",
        strtoupper($uniqueId));
      if ($elc13 !== DB_EMPTY_SET) {
        return 'ELC1-3';
      } else {
        return '';
      }
    }
  }

  private function updateConditionalAdmitProgramCode($uniqueId, $code) {
    if (!$this->dbh) {
      $this->dbh = $this->database->getHandle($this->datasource_name);
    }
    
    switch($code) {
      case 'ACE':
        $sql = 'BEGIN baninst1.PZ_ACE_Load(:uniqueId); end;';
        break;
      case 'ELC4':
        $sql = 'BEGIN baninst1.PZ_ELC4_Load(:uniqueId); end;';
        break;
      case 'ELC5':
        $sql = 'BEGIN baninst1.PZ_ELC5_Load(:uniqueId); end;';
        break;
    }

    $sth = $this->dbh->prepare($sql);
    
    $sth->bind_by_name(":uniqueId", $uniqueId, -1);
    
    $sth->execute();
  }

  public function updateStudent() {
    $request = $this->getRequest();
    $response = $this->getResponse();
    $data = $request->getData();
    
    $uniqueId = $request->getResourceParam('uniqueId');
    
    $currentConditionalAdmitProgramCode = $this->getConditionalAdminProgramCode($uniqueId);
    $newConditionalAdmitProgramCode = $data['conditionalAdmitProgramCode'];
    
    if ($currentConditionalAdmitProgramCode == 'ACE' &&
        $newConditionalAdmitProgramCode == '') {
      $this->updateConditionalAdmitProgramCode($uniqueId, 'ACE');
    } else if ($currentConditionalAdmitProgramCode == 'ELC1-3' &&
               $newConditionalAdmitProgramCode == 'ELC4') {
      $this->updateConditionalAdmitProgramCode($uniqueId, 'ELC4');
    } else if (($currentConditionalAdmitProgramCode == 'ELC1-3' ||
                $currentConditionalAdmitProgramCode == 'ELC4') &&
               $newConditionalAdmitProgramCode == '') {
      $this->updateConditionalAdmitProgramCode($uniqueId, 'ELC5');
    } else {
      $errorMsg = sprintf('Invalid initial student state for program change - %s to %s',
        ($currentConditionalAdmitProgramCode == '' ? 'Normal' : $currentConditionalAdmitProgramCode),
        ($newConditionalAdmitProgramCode == '' ? 'Normal' : $newConditionalAdmitProgramCode));
      $response->setPayload(array('message' => $errorMsg));
      $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
      return $response;
    }

    $response->setStatus(\MiamiOH\RESTng\App::API_OK);

    return $response;
  }
  
  public function setDatabase($database) {
    $this->database = $database;
  }
}
