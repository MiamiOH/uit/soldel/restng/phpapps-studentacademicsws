<?php

namespace MiamiOH\PhpAppsStudentAcademicWs\Services;

class FieldOfStudy extends \MiamiOH\RESTng\Service {

  private $datasource_name = 'MUWS_GEN_PROD';

  public function getFieldOfStudy() {
    $request = $this->getRequest();
    $response = $this->getResponse();
    
    $uniqueId = $request->getResourceParam('uniqueId');
    $degId    = $request->getResourceParam('degId');

    $authUser = $this->getApiUser()->getUsername();

    if (strtolower($authUser) != strtolower($uniqueId)) {
      if (!$this->getApiUser()->isAuthorized('WebServices', 'StudentFieldOfStudy', 'view') &&
          !$this->getApiUser()->isAuthorized('WebServices', 'StudentFieldOfStudy', 'All')) {
        $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
        return $response;
      }
    }
    
    $dbh = $this->database->getHandle($this->datasource_name);
    $dbh->mu_trigger_error = false;

    $pidm = $dbh->queryfirstcolumn("SELECT szbuniq_pidm FROM szbuniq WHERE szbuniq_unique_id = ?", strtoupper($uniqueId));
    if ($pidm === DB_EMPTY_SET) {
      $response->setPayload(array('message' => 'User not found'));
      $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
      return $response;
    }

    $fieldsOfStudy = $dbh->queryall_array(
      "SELECT sorlfos_majr_code            AS id,
              nvl(sorlfos_majr_code , ' ') AS code,
              nvl(stvmajr_desc      , ' ') AS name,
              nvl(sorlfos_lfst_code , ' ') as type_code
         FROM sorlfos
    LEFT JOIN stvmajr
           ON stvmajr_code = sorlfos_majr_code
        WHERE sorlfos_pidm       = ?
          AND sorlfos_lcur_seqno = ?
          AND sorlfos_cact_code = 'ACTIVE'
          AND sorlfos_lfst_code in ('MAJOR', 'MINOR')
          AND sorlfos_term_code = 
            (SELECT MAX(sorlfos_term_code)
               FROM sorlfos
              WHERE sorlfos_pidm       = ?
                AND sorlfos_term_code <= fz_get_term())
          ORDER BY sorlfos_seqno",
      $pidm,
      $degId,
      $pidm);

    $count = -1;
    $returnArray = array();
    foreach($fieldsOfStudy as $fieldOfStudy) {
      $count++;
      $returnArray[$count]['id']       = trim($fieldOfStudy['id']);
      $returnArray[$count]['code']     = trim($fieldOfStudy['code']);
      $returnArray[$count]['name']     = trim($fieldOfStudy['name']);
      $returnArray[$count]['typeCode'] = trim(strtolower($fieldOfStudy['type_code']));

      $sfosResponse = $this->callResource('student.academics.v1.subfieldOfStudy',
        array('params' => array('uniqueId' => $uniqueId, 
                                'degId'    => $degId,
                                'fosId'    => $fieldOfStudy['id'])));
      if ($sfosResponse->getStatus() !== \MiamiOH\RESTng\App::API_OK) {
        $response->setPayload(array('message' => 'User not found when retrieving subfield of study'));
        $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        return $response;
      }
      $returnArray[$count]['subfieldOfStudy'] = $sfosResponse->getPayload();
    }

    $response->setStatus(\MiamiOH\RESTng\App::API_OK);
    $response->setPayload($returnArray);

    return $response;
  }
  
  public function setDatabase($database) {
    $this->database = $database;
  }
}
