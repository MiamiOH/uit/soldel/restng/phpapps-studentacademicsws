<?php

namespace MiamiOH\PhpAppsStudentAcademicWs\Services;

class subfieldOfStudy extends \MiamiOH\RESTng\Service {

  private $datasource_name = 'MUWS_GEN_PROD';

  public function getsubFieldOfStudy() {
    $request = $this->getRequest();
    $response = $this->getResponse();
    
    $uniqueId = $request->getResourceParam('uniqueId');
    $degId    = $request->getResourceParam('degId');
    $fosId    = $request->getResourceParam('fosId');

    $authUser = $this->getApiUser()->getUsername();

    if (strtolower($authUser) != strtolower($uniqueId)) {
      if (!$this->getApiUser()->isAuthorized('WebServices', 'StudentSubfieldOfStudy', 'view') &&
          !$this->getApiUser()->isAuthorized('WebServices', 'StudentSubfieldOfStudy', 'All')) {
        $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
        return $response;
      }
    }
    
    $dbh = $this->database->getHandle($this->datasource_name);
    $dbh->mu_trigger_error = false;

    $pidm = $dbh->queryfirstcolumn("SELECT szbuniq_pidm FROM szbuniq WHERE szbuniq_unique_id = ?", strtoupper($uniqueId));
    if ($pidm === DB_EMPTY_SET) {
      $response->setPayload(array('message' => 'User not found'));
      $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
      return $response;
    }

    $subfieldsOfStudy = $dbh->queryall_array(
      "SELECT sorlfos_majr_code            AS id,
              nvl(sorlfos_majr_code , ' ') AS code,
              nvl(stvmajr_desc      , ' ') AS name,
              CASE sorlfos_seqno
                WHEN 2 THEN 'concentration'
                WHEN 3 THEN 'thematicSequence'
                ELSE 'UNKNOWN' END         AS type_code,
              CASE sorlfos_seqno
                WHEN 2 THEN 'Concentration'
                WHEN 3 THEN 'Thematic Sequence'
                ELSE 'Unknown' END         AS type_name
         FROM sorlfos
    LEFT JOIN stvmajr
           ON stvmajr_code = sorlfos_majr_code
        WHERE sorlfos_pidm       = ?
          AND sorlfos_lcur_seqno = ?
          AND sorlfos_majr_code_attach = ?
          AND sorlfos_cact_code = 'ACTIVE'
          AND sorlfos_lfst_code = 'CONCENTRATION'
          AND sorlfos_term_code = 
            (SELECT MAX(sorlfos_term_code)
               FROM sorlfos
              WHERE sorlfos_pidm       = ?
                AND sorlfos_term_code <= fz_get_term())
          ORDER BY sorlfos_seqno",
      $pidm,
      $degId,
      $fosId,
      $pidm);

    $count = -1;
    $returnArray = array();
    foreach($subfieldsOfStudy as $subfieldOfStudy) {
      $count++;
      $returnArray[$count]['id']         = trim($subfieldOfStudy['id']);
      $returnArray[$count]['code']       = trim($subfieldOfStudy['code']);
      $returnArray[$count]['name']       = trim($subfieldOfStudy['name']);
      $returnArray[$count]['typeCode']   = trim($subfieldOfStudy['type_code']);
      $returnArray[$count]['typeName']   = trim($subfieldOfStudy['type_name']);
    }

    $response->setStatus(\MiamiOH\RESTng\App::API_OK);
    $response->setPayload($returnArray);

    return $response;
  }
  
  public function setDatabase($database) {
    $this->database = $database;
  }
}
