<?php

namespace MiamiOH\PhpAppsStudentAcademicWs\Services;

class Attributes extends \MiamiOH\RESTng\Service
{

    private $datasource_name = 'MUWS_GEN_PROD';

    public function getAttributes()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $uniqueId = $request->getResourceParam('uniqueId');

        $authUser = $this->getApiUser()->getUsername();

        $options = $request->getOptions();
        if (!isset($options['mode'])) {
            $options['mode'] = 'LEARNER';
        }

        if (!isset($options['termCode'])) {
            $options['termCode'] = 'current';
        }
        if ($options['termCode'] &&
            $options['termCode'] != 'current' &&
            $options['termCode'] != 'max' &&
            !preg_match('/^\d\d\d\d\d\d$/', $options['termCode'])) {
            $response - setPayload(array('message' => 'Invalid termCode option'));
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            return $response;
        }

        if (strtolower($authUser) != strtolower($uniqueId)) {
            if (!$this->getApiUser()->isAuthorized('WebServices', 'StudentAttributes', 'view') &&
                !$this->getApiUser()->isAuthorized('WebServices', 'StudentAttributes', 'All')) {
                $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
                return $response;
            }
        }

        $dbh = $this->database->getHandle($this->datasource_name);
        $dbh->mu_trigger_error = false;

        $pidm = $dbh->queryfirstcolumn("SELECT szbuniq_pidm FROM szbuniq WHERE szbuniq_unique_id = ?",
            strtoupper($uniqueId));
        if ($pidm === DB_EMPTY_SET) {
            $response->setPayload(array('message' => 'User not found'));
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        if (!$options['termCode'] || $options['termCode'] == 'current') {
            $attributes = $dbh->queryall_array(
                "SELECT sgrsatt_atts_code          AS id,
                sgrsatt_atts_code          AS code,
                NVL(sgvsatt_attr_desc,' ') AS name,
                sgrsatt_term_code_eff      AS termcode
           FROM sgrsatt
      LEFT JOIN sgvsatt
             ON sgvsatt_pidm = sgrsatt_pidm
            AND sgvsatt_term_code = sgrsatt_term_code_eff
            AND sgvsatt_attr_code = sgrsatt_atts_code
          WHERE sgrsatt_pidm = ?
            AND sgrsatt_term_code_eff = (
              SELECT max(sgrsatt_term_code_eff)
                FROM sgrsatt
               WHERE sgrsatt_pidm = ?
                 AND sgrsatt_term_code_eff <= fz_get_term())",
                $pidm,
                $pidm);
        } else {
            if ($options['termCode'] == 'max') {
                $attributes = $dbh->queryall_array(
                    "SELECT sgrsatt_atts_code          AS id,
                sgrsatt_atts_code          AS code,
                NVL(sgvsatt_attr_desc,' ') AS name,
                sgrsatt_term_code_eff      AS termcode
           FROM sgrsatt
      LEFT JOIN sgvsatt
             ON sgvsatt_pidm = sgrsatt_pidm
            AND sgvsatt_term_code = sgrsatt_term_code_eff
            AND sgvsatt_attr_code = sgrsatt_atts_code
          WHERE sgrsatt_pidm = ?
            AND sgrsatt_term_code_eff = (
              SELECT max(sgrsatt_term_code_eff)
                FROM sgrsatt
               WHERE sgrsatt_pidm = ?)",
                    $pidm,
                    $pidm);
            } else {
                $attributes = $dbh->queryall_array(
                    "SELECT sgrsatt_atts_code          AS id,
                sgrsatt_atts_code          AS code,
                NVL(sgvsatt_attr_desc,' ') AS name,
                sgrsatt_term_code_eff      AS termcode
           FROM sgrsatt
      LEFT JOIN sgvsatt
             ON sgvsatt_pidm = sgrsatt_pidm
            AND sgvsatt_term_code = sgrsatt_term_code_eff
            AND sgvsatt_attr_code = sgrsatt_atts_code
          WHERE sgrsatt_pidm = ?
            AND sgrsatt_term_code_eff = (
              SELECT max(sgrsatt_term_code_eff)
                FROM sgrsatt
               WHERE sgrsatt_pidm = ?
                 AND sgrsatt_term_code_eff <= ?)",
                    $pidm,
                    $pidm,
                    $options['termCode']);
            }
        }


        $returnArray = array();
        $count = -1;
        foreach ($attributes as $attribute) {
            $count++;
            $returnArray[$count]['id'] = trim($attribute['id']);
            $returnArray[$count]['code'] = trim($attribute['code']);
            $returnArray[$count]['name'] = trim($attribute['name']);
            $returnArray[$count]['startingTermCode'] = trim($attribute['termcode']);
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($returnArray);

        return $response;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }
}
