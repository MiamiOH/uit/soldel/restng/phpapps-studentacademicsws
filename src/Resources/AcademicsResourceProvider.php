<?php

namespace MiamiOH\PhpAppsStudentAcademicWs\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class AcademicsResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'StudentAcademics',
            'class' => 'MiamiOH\PhpAppsStudentAcademicWs\Services\Student',
            'description' => 'Top level service for student academic information',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));

        $this->addService(array(
            'name' => 'StudentDegree',
            'class' => 'MiamiOH\PhpAppsStudentAcademicWs\Services\Degree',
            'description' => 'Academic Degrees for a student',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));

        $this->addService(array(
            'name' => 'StudentFieldOfStudy',
            'class' => 'MiamiOH\PhpAppsStudentAcademicWs\Services\FieldOfStudy',
            'description' => 'Majors/minors for a student',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));

        $this->addService(array(
            'name' => 'StudentSubfieldOfStudy',
            'class' => 'MiamiOH\PhpAppsStudentAcademicWs\Services\SubfieldOfStudy',
            'description' => 'Concentrations/Thematic sequences for a student',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));

        $this->addService(array(
            'name' => 'StudentAttributes',
            'class' => 'MiamiOH\PhpAppsStudentAcademicWs\Services\Attributes',
            'description' => 'Academic Attributes for a student',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.student',
            'description' => 'Get student academic information',
            'pattern' => '/student/academics/v1/:uniqueId',
            'service' => 'StudentAcademics',
            'method' => 'getStudent',
            'returnType' => 'model',
            'params' => array(
                'uniqueId' => array('description' => 'uniqueId of the student'),
            ),
            'options' => array(
                'mode' => array('description' => 'record mode to return - defaults to LEARNER'),
                'termCode' => array('description' => 'termCode to use to collect data - defaults to "current"')
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token')
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'student.academics.v1.student.update',
            'description' => 'Update student academic information',
            'pattern' => '/student/academics/v1/:uniqueId',
            'service' => 'StudentAcademics',
            'method' => 'updateStudent',
            'returnType' => 'model',
            'params' => array(
                'uniqueId' => array('description' => 'uniqueId of the student'),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    'application' => 'WebServices',
                    'module' => 'StudentAcademics',
                    'key' => 'update'
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.degree',
            'description' => 'Get student degree information',
            'pattern' => '/student/academics/v1/:uniqueId/degree',
            'service' => 'StudentDegree',
            'method' => 'getDegree',
            'returnType' => 'collection',
            'params' => array(
                'uniqueId' => array('description' => 'uniqueId of the student'),
            ),
            'options' => array(
                'mode' => array('description' => 'record mode to return - defaults to LEARNER'),
                'termCode' => array('description' => 'termCode to use to collect data - defaults to "current"')
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token')
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.fieldOfStudy',
            'description' => 'Get student majors/minors information',
            'pattern' => '/student/academics/v1/:uniqueId/fieldOfStudy/:degId',
            'service' => 'StudentFieldOfStudy',
            'method' => 'getFieldOfStudy',
            'returnType' => 'collection',
            'params' => array(
                'uniqueId' => array('description' => 'uniqueId of the student'),
                'degId' => array('description' => 'id of degree'),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token')
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.subfieldOfStudy',
            'description' => 'Get student concentration/thematic sequence information',
            'pattern' => '/student/academics/v1/:uniqueId/subfieldOfStudy/:degId/:fosId',
            'service' => 'StudentSubfieldOfStudy',
            'method' => 'getSubfieldOfStudy',
            'returnType' => 'collection',
            'params' => array(
                'uniqueId' => array('description' => 'uniqueId of the student'),
                'degId' => array('description' => 'id of degree'),
                'fosId' => array('description' => 'id of field of study'),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token')
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'student.academics.v1.attributes',
            'description' => 'Get student attributes',
            'pattern' => '/student/academics/v1/:uniqueId/attributes',
            'service' => 'StudentAttributes',
            'method' => 'getAttributes',
            'returnType' => 'collection',
            'params' => array(
                'uniqueId' => array('description' => 'uniqueId of the student'),
            ),
            'options' => array(
                'termCode' => array('description' => 'termCode to use to collect data - defaults to "current"')
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token')
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}