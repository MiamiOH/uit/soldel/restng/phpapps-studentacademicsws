# Attention: your project has been converted from Subversion to Git
## Please review the following information before working on your project.

Your project was converted from Subversion to Git using the svn2git tool.  Please review the contents of the repository before continuing work.  If you were working on a branch in Subversion, that branch will be replicated here as its own branch, but whatever was in trunk in Subversion will be in the master branch.

## Tags
A common pattern for Subversion tags at Miami was to have a tags/V1.0 directory and within it the actual versions like V1.0.1, V1.0.2, etc.  The migration process was unable to keep this pattern, so your tag list may have been flattened.  Due to this and some non-standard tagging patterns, there was opportunity for naming conflicts and other errors that may have caused your tags to be renamed.  If your project depends on tags, please review the tag list before continuing work.

## README.md
A README.md file has been added to the root of your project (you are reading it right now).  If this causes problems for the deployment of your project, please consider re-organizing your project so that it can remain.  For instance, if this is a web development project and the root of this project gets served via the webserver, consider re-organizing this project to have a www/ directory in the root and place your web files there.  This makes it so that the root of your project can contain directories like sql/, docs/, and tests/ that don't get deployed to the webserver. You will need to adjust your deployment instructions as appropriate.

This README.md file is also displayed in GitLab when browsing projects.  Consider replacing the content of this document with something appropriate to your project, like a summary, simple documentation, links to more complex documentation, deployment instructions, etc.

## Special consideration for PHP/ColdFusion projects
As part of the move to Git, we have restructured the projects on the shared development servers.  All clones of git projects will be created in /opt/webapps/wc and a symlink will be created to the appropriate web directory of your project in /opt/webapps/phpapps or /opt/webapps/cfapps.  If you need to make your own personal copy, you can do that following something similar to these instructions:

```
cd /opt/webapps/wc
git clone git@git.itapps.miamioh.edu:solution-delivery-dev/your-project.git your-project.uniqueid
ln -s /opt/webapps/wc/your-project.uniqueid/www /opt/webapps/phpapps/your-project.uniqueid
```

Please note that the test and production systems have not been updated to this new style.  If you are going to follow this style for your project in test and production, you will need to have the above steps done a single time during your next deployment, remembering to archive/remove the project directory in /opt/webapps/phpapps or /opt/webapps/cfapps first.

## Git Resources
* [Main Git/GitLab @ Miami Page](https://wiki.miamioh.edu/confluence/display/Doc/Git+and+GitLab)
* [Mastering Git Video Training](https://wiki.miamioh.edu/confluence/display/Doc/Mastering+Git+Video+Collection)
* [Git Workbook](https://wiki.miamioh.edu/confluence/display/Doc/Git+Workbook)
